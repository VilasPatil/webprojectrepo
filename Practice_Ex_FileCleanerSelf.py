import os
path="."
files=os.listdir(path)

NewFolderName="Junk"
if not os.path.exists(path + "\\" +NewFolderName):
    os.mkdir(path + "\\" +NewFolderName)
#print(files)

def createIfNotExists(foldername,path):
    if not os.path.exists(path +"\\" + NewFolderName +"\\" + foldername):
        os.mkdir(path +"\\" + NewFolderName+ +"\\" + foldername)
def move(foldername,files):
    for file in files:
        os.replace(path + "\\" + file, path +"\\" + NewFolderName +"\\" + foldername + "\\"+ file)

createIfNotExists("Images",path)
createIfNotExists("Docs",path)
createIfNotExists("Media",path)
createIfNotExists("Others",path)
createIfNotExists("ZipFolders",path)
createIfNotExists("DBFiles",path)

excludelist=[".py",".cfg"]
imgexts=[".png",".jpg",".jpeg"]
docexts=[".doc",".txt",".docx",".pdf",".xlsx"]
medexts=[".mp4",".tvs",".mp3",".flv"]
zipexts=[".zip",".rar"]
dbexts=[".sql",".mdf",".ldf",".bak"]
#imgs=[]
#for file in files:
#    if os.path.splitext(file)[1].lower() in imgexts:
#        imgs.append(file)
imgs=[file for file in files if os.path.splitext(file)[1].lower() in imgexts]
docs=[file for file in files if os.path.splitext(file)[1].lower() in docexts]
medias=[file for file in files if os.path.splitext(file)[1].lower() in medexts]
zips=[file for file in files if os.path.splitext(file)[1].lower() in zipexts]
dbfiles=[file for file in files if os.path.splitext(file)[1].lower() in dbexts]
oths=[]
for file in files:
    ext = os.path.splitext(file)[1].lower()
    if (ext not in dbexts) and (ext not in excludelist) and (ext not in imgexts) and (ext not in docexts) and (ext not in medexts) and (ext not in zipexts) and os.path.isfile(path + "\\" + file):
        oths.append(file)

move("Images",imgs)
move("Media",medias)
move("ZipFolders",zips)
move("Docs",docs)
move("DBFiles",dbfiles)
move("Others",oths)

print(f"Your all junk files moved to {os.getcwd()}\\{NewFolderName}")