import time


def fibno(n):
    a=0
    b=1
    if(n==1):
        print(a)
    else:
        print(a)
        print(b)
        for i in range(2,n):
            print(f"val of i is {i}")
            c=a+b
            a=b
            b=c
            if c>n:
                break;
            else:
                print(c)

def fact(n):
    c=1
    for i in range(1,n+1):
        c=c*i
    return c

def fibiter(n):
    prevno=0
    currentno=1
    print(prevno)
    print(currentno)
    for i in range(1,n):
        preprevno=prevno
        prevno=currentno
        currentno=prevno+preprevno
        print(currentno)
    return currentno

def fibseries(n):
    if n ==0:
        return 0
    elif n ==1:
        return 1
    else:
        return fibseries(n-1)+fibseries(n-2)

def factr(n):
    if n==0:
        return 1
    else:
        return n*factr(n-1)
initm=time.time()
no =int(input("Enter the number :"))
#for factorial recursive -- optimized
c=fact(no)
print("factorial no =",c)
#for factorial manual
d=factr(no)
print("factorial no =",d)
print(f"it took {time.time()-initm} to complete")
#for fib series recursive --not optimized
e=fibseries(no)
print(f"Fib series result :{e}")
#fibno(no)
#for fib series manual --optimized
e=fibiter(no)
print(f"Fib series result :{e}")
print(f"it took {time.time()-initm} to complete")
