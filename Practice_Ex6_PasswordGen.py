import string
import random
s1=string.ascii_uppercase
s2=string.ascii_lowercase
s3=string.digits
s4=string.punctuation
s=[]
s.extend(list(s1))
s.extend(list(s2))
s.extend(list(s3))
s.extend(list(s4))
print(s)
passlen=input("Enter the password length :\n")
if passlen.isdigit():
    passlen=int(passlen)
    print("Your password without shuffle is: ", end="")
    print("".join(random.sample(s,passlen)))
    random.shuffle(s)
    print("Your password with shuffle is: ", end="")
    print("".join(s[0:passlen]))
else:
    print("Please enter the valid number")
