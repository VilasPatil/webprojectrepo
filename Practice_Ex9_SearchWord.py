import os

filelist = os.listdir()
def searchword(filename,keyword):
    with open(filename,"r") as f:
        readcontent=f.read()
        if keyword.lower() in readcontent.lower():
            return True
            #print(f"***keyword {keyword} found ")
        else:
            #print(f"keyword {keyword} not found")
            return False


if __name__ == '__main__':
    while(True):
        keyword=input("Enter the keyword to search Q to quit: \n")
        if keyword.lower()=="q":
            break
        else:
            kcount=0
            fileswithkeyword=[]
            print(f"Detecting {keyword} .....")
            for file in filelist:
                if file.endswith(".txt"):
                    flag = searchword(file,keyword)
                    if flag==True:
                        kcount=kcount+1
                        fileswithkeyword.append(file)
                        print(f"***keyword {keyword} found ")
                    else:
                        print(f"keyword {keyword} not found")
            print("#######Search Summery#########")
            print(f"Keyword {keyword} found in {kcount} files :")
            for x,y in enumerate(fileswithkeyword):
                print(x+1,y)