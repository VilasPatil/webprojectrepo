from flask import Flask,render_template,request,session,redirect
from flask_sqlalchemy import SQLAlchemy
import json
import math
import mysql.connector
from flask_mail import Mail
import os
from werkzeug.utils import secure_filename

local_server=True
with open("config.json","r") as f:
    params=json.load(f)["params"]

mydb=mysql.connector.connect(
    host=params["host"],
    user=params["user"],
    passwd=params["passwd"],
    database=params["database"]
)
mycursor=mydb.cursor()

from datetime import datetime
app=Flask(__name__)
app.secret_key = 'super-secret-key'
app.config['upload_folder']=params['upload_location']
app.config.update(
    MAIL_SERVER='smtp.gmail.com',
    MAIL_PORT='465',
    MAIL_USE_SSL=True,
    MAIL_USERNAME=params['gmail-user'],
    MAIL_PASSWORD=params['gmail-password']
)
mail=Mail(app)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
if local_server:
    app.config['SQLALCHEMY_DATABASE_URI'] = params["local_uri"]
else:
    app.config['SQLALCHEMY_DATABASE_URI'] = params["prod_uri"]

db = SQLAlchemy(app)
print(db.Model)

class Contacts(db.Model):
    '''
    sno, name phone_num, msg, date, email
    '''
    srno = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    phone_num = db.Column(db.String(12), nullable=False)
    msg = db.Column(db.String(120), nullable=False)
    date = db.Column(db.String(12), nullable=True)
    email = db.Column(db.String(20), nullable=False)

class Posts(db.Model):
    '''
    sno, name phone_num, msg, date, email
    '''
    srno = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), nullable=False)
    slug = db.Column(db.String(21), nullable=False)
    content = db.Column(db.String(120), nullable=False)
    date = db.Column(db.String(12), nullable=True)
    img_file = db.Column(db.String(20), nullable=False)


def insert_contact(emp):
    sql="insert into Contacts(name,phone_num,msg,date,email) values(%s,%s,%s,%s,%s)"
    values=(emp.name,emp.phone_num,emp.msg,emp.date,emp.email)
    mycursor.execute(sql,values)
    mydb.commit()
    #mycursor.execute("insert into employee values(%s,%s,%s)",(emp.first,emp.last,emp.pay))

def insert_posts(post):
    sql="insert into posts(title,slug,content,img_file,date) values(%s,%s,%s,%s,%s)"
    values=(post.title,post.slug,post.content,post.img_file,post.date)
    mycursor.execute(sql,values)
    print(values)
    mydb.commit()

def updatepost(post):
    val = (post.title,post.slug,post.content,post.img_file,post.date,post.srno)
    mycursor.execute("update posts set title=%s,slug=%s,content=%s,img_file=%s,"
                     "date=%s where srno=%s",val)
    postsrec = mycursor.fetchone()
    mydb.commit()
    # img_file='home-bg.jpg'


def getpostbyno(srno):
    val = (srno,)
    mycursor.execute("SELECT * FROM posts where srno=%s",val)
    postsrec = mycursor.fetchone()
    #for post in postsrec:
    srno=postsrec[0]
    title = postsrec[1]
    slug = postsrec[2]
    content = postsrec[3]
    img_file = postsrec[4]
    print(title)
    # img_file='home-bg.jpg'
    posts=Posts(srno=srno,title=title, slug=slug, content=content, date=datetime.now(), img_file=img_file)
    print("content:")
    print(posts.content)
    return posts
def deletepostbyno(srno):
    val = (srno,)
    mycursor.execute("delete from posts where srno=%s", val)
    mydb.commit()

def getallrecords():
    mycursor.execute("SELECT * FROM posts")
    postsrec = mycursor.fetchall()
    posts = []
    for post in postsrec:
        srno=post[0]
        title = post[1]
        slug = post[2]
        content = post[3]
        img_file = post[4]
        print(title)
        # img_file='home-bg.jpg'
        posts.append(Posts(srno=srno,title=title, slug=slug, content=content, date=datetime.now(), img_file=img_file))
    return posts
@app.route("/")
def home():
    mycursor.execute("SELECT * FROM posts")
    postsrec = mycursor.fetchall()
    posts = []
    for post in postsrec:
        title = post[0]
        title = post[1]
        slug = post[2]
        content = post[3]
        img_file = post[4]
        print(title)
        # img_file='home-bg.jpg'
        posts.append(Posts(title=title, slug=slug, content=content, date=datetime.now(), img_file=img_file))
    last= math.ceil(len(posts)/int(params['no_of_posts']))

    page=request.args.get('page')
    if (not str(page).isdigit()):
        page=1
    page = int(page)
    posts=posts[(page-1)*int(params['no_of_posts']):(page-1)*int(params['no_of_posts'])+int(params['no_of_posts'])]
    if (page==1):
        prev="#"
        next="/?page="+str(page+1)
    elif (page==last):
        prev="/?page="+str(page-1)
        next="#"
    else:
        prev = "/?page=" + str(page - 1)
        next="/?page="+str(page+1)



    return render_template('index.html',params=params,posts=posts,prev=prev,next=next)


@app.route("/about")
def about():
    return render_template('about.html',params=params)
@app.route("/logout")
def logout():
    session.pop('user')
    return redirect('/dashboard')

@app.route("/uploader", methods=['GET','POST'])
def uploader():
    if ('user' in session and session['user'] == params['Admin_user']):
        if request.method=='POST':
            f=request.files['file1']
            f.save(os.path.join(app.config['upload_folder'],secure_filename(f.filename)))
            return "Uploaded succssfully"
    return render_template('about.html',params=params)
@app.route("/dashboard", methods=['GET','POST'])
def dashboard():
    print(params['Admin_user'])
    print(params['Admin_pwd'])
    if ('user' in session and session['user']==params['Admin_user']):
        posts = getallrecords()
        return render_template('dashboard.html',params=params,posts=posts)

    if request.method=='POST':
        username = request.form.get('uname')
        pwd = request.form.get('password')
        if (username ==params['Admin_user']  and pwd==params['Admin_pwd']):
            session['user']=username
            posts = getallrecords()
            return render_template('dashboard.html',params=params,posts=posts)

    return render_template('login.html',params=params)

@app.route("/post/<string:post_slug>",methods=['GET'])
def post_route(post_slug):
    #post=Posts.query.filter_by(slug=post_slug).first()
    val = (post_slug,)
    mycursor.execute("SELECT * FROM posts where slug=%s", val)
    post=mycursor.fetchone()
    title=post[1]
    slug=post[2]
    content = post[3]
    img_file = post[4]
    #img_file='home-bg.jpg'
    post = Posts(title=title, slug = slug, content = content, date= datetime.now(),img_file = img_file)
    return render_template('post.html',params=params,post=post)

@app.route("/delete/<string:srno>", methods = ['GET', 'POST'])
def delete(srno):
    if ('user' in session and session['user'] == params['Admin_user']):
        post = deletepostbyno(srno)
        return redirect('/dashboard')

@app.route("/edit/<string:srno>", methods = ['GET', 'POST'])
def edit(srno):
    if ('user' in session and session['user'] == params['Admin_user']):
        print(srno)
        print(request.method)
        if (request.method == 'POST'):
            title = request.form.get('title')
            #tline = request.form.get('tline')
            slug = request.form.get('slug')
            content = request.form.get('content')
            img_file = request.form.get('img_file')
            print(srno)
            if (srno=='0'):
                print(srno)
                post=Posts(title=title, slug = slug, content = content, date= datetime.now(),img_file = img_file)
                insert_posts(post)
                post = getallrecords()
                return render_template('dashboard.html',params=params,posts=post)
            else:
                post=getpostbyno(srno)
                post.title=title
                post.slug = slug
                post.content = content
                post.img_file = img_file
                post.date = datetime.now()
                updatepost(post)
                return redirect('/edit/'+srno)

        if(srno == '0'):
            print(srno)
            post=Posts(srno=0,title="", slug="", content="", date=datetime.now(), img_file="")
        else:
            post = getpostbyno(srno)
        print(post.content)
        return render_template("edit.html",params=params,post=post)

@app.route("/contact", methods = ['GET', 'POST'])
def contact():
    print(request.method)
    if(request.method=='POST'):
        '''Add entry to the database'''
        name = request.form.get('name')
        email = request.form.get('email')
        phone = request.form.get('phone')
        message = request.form.get('message')
        entry = Contacts(name=name, phone_num = phone, msg = message, date= datetime.now(),email = email )
        insert_contact(entry)
        mail.send_message('New message from ' + name,sender=email,
                          recipients=[params['gmail-user']],body=message + "\n" + phone)
        #db.session.add(entry)
        #db.session.commit()

    return render_template('contact.html',params=params)

app.run(debug=True)
