--SELECT * from StagingDB_Sonak.dbo.in_pos_article_master WHERE  MATNR ='ONIT-1183A701017' -- NOT IN(SELECT ITEMNUMBER from ProductRSF) --'ONIT-1182A030001'
--select * from ProductRSF where ItemNumber='ONIT-1183A701017'
--select * FROM ALIASNUMBER where ItemNumber='ONIT-1183A701017'
--select * FROM SalesPriceList where ItemNumber='ONIT-1183A701017'
create proc dumpdata
as
begin
DECLARE @TimeStamp NUMERIC(18)  
declare @company int,@division nvarchar(10), @facility nvarchar(10) 
select @company=company,@division=Division,@facility=Facility from facility
SET @TIMESTAMP = REPLACE((CONVERT(char(12), GETDATE(), 112) + REPLACE(CONVERT(varchar(8), GETDATE(), 108), ':', '')), ' ', '') 



 insert into ProductRSF  
   ([Company],[ItemStatus],[ItemNumber],[Name],[Description],[DrawingNumber],[QuantityDecimals],[BasicUnitMeasure],  
   [ItemGroup],[ProductGroup],[BusinessArea],[EnvironmentGroup],[ItemType],[ItemCategory],[HierarchyLevel1],[HierarchyLevel2],  
   [HierarchyLevel3],[HierarchyLevel4],[HierarchyLevel5],[SearchGroup1],[SearchGroup2],[SearchGroup3],[SearchGroup4],  
   [SearchGroup5],[ConfigurationCode],[VatCode],[FreeField1],[FreeField2],[FreeField3],[FreeField4],[FreeField5],  
   [ConsumptionCode],[ProcurementGroup],[LotControlMethod],[UseAUM],[PriceDecimals],[GroupTechClass],[DistrGroupTech],  
   [SaleItem],[TaxCode],[SalesPriceQuantity],[SalesPriceCurrency],[SalesPriceUOM],[SupplierNumber],[MakeBuyCode],  
   [DiscountGroupItem],[StatisticsIdentity1],[AlternateUOM],[AlternateUOMType],[AlternateSalesUOM],[AlternateStatUOM],  
   [AlternateSalesPriceUOM],[SalesPrice],[SalesTaxItem],[ChargeModel],[PriceList],[MRPItem],[Volume],[NetWeight],  
   [GrossWeight],[Specification1],[Specification2],[Specification3],[Specification4],[StyleNo],[DimensionXSeqNo],  
   [DimensionX],[DimensionXOptionID],[DimensionYSeqNo],[DimensionY],[DimensionYOptionID],[DimensionZSeqNo],  
   [DimensionZ],[DimensionZOptionID],[Hierarchy],[CreateDate],[CreateTime],[ChangeNo],[ChangedBy],  
   [StoreTimeStamp],[EASTimeStamp],[BatchNumber]) 
--select DISTINCT 100, Case when WO.SPVBC = 'Y' then '90' else '20' end, WO.MATNR, Left(WO.MAKTM,30),WO.MAKTM, '',0,WO.MEINH,  
  select DISTINCT @company, Case when WO.SPVBC = 'Y' then '90' else '20' end, WO.MATNR, Left(WO.MAKTM,30),WO.MAKTM, '',0,WO.MEINH,  
   WO.MTART,'NA', 'NA','0',WO.MWSKZ,0,'','',  
   '','','','','','','',  
   '',3,0,wo.AENKZ,0,'',WO.MATKL,'',  
   '0','NA',0,1,0,'NA','NA',  
   1,'',0,'0','NA','',1,  
   'NA','','',0,0,0,  
   '0',WO.KWERT,0,'0','0',0,0,0,  
   0,'','','','','','',  
   '','','','','','',  
   '','','',MAX(WO.CREDAT),MAX(WO.CRETIM),1,'SAPMGP',  
   0,@TIMESTAMP,'0'  
   
   from StagingDB_Sonak.dbo.in_pos_article_master  WO  WHERE MATNR='ONIT-1183A701017' and KWERT>'0'
   GROUP by WO.SPVBC,WO.MATNR, Left(WO.MAKTM,30),WO.MAKTM,WO.MEINH,  
   WO.MTART,WO.MWSKZ,wo.AENKZ,WO.MATKL,WO.KWERT



insert into AliasNumber  
   ( [Company],[AliasCategory],[AliasQualifier],[ItemNumber],[AliasNumber],  
   [Partner],[ValidFrom],[ValidTo],[Season],[CreateDate],  
   [CreateTime],[ChangeNo],[ChangedBy],[StoreTimeStamp],[EASTimeStamp],[BatchNumber])
select DISTINCT @company, 2,WO.EANTP,WO.MATNR,WO.EAN11,  
   '0',0,0,WO.HPEAN,MAX(WO.CREDAT),MAX(WO.CRETIM),
   1,'SAPMGP',0,@TIMESTAMP,'0'     
   from StagingDB_Sonak.dbo.in_pos_article_master  WO  WHERE MATNR='ONIT-1183A701017' AND EAN11>'0'
   GROUP by  WO.KSCHL,WO.EANTP,WO.MATNR,WO.EAN11,WO.HPEAN




--DECLARE @TimeStamp NUMERIC(18)  
--declare @company int,@division nvarchar(10), @facility nvarchar(10) 
--select @company=company,@division=Division,@facility=Facility from facility
--SET @TIMESTAMP = REPLACE((CONVERT(char(12), GETDATE(), 112) + REPLACE(CONVERT(varchar(8), GETDATE(), 108), ':', '')), ' ', '') 

 insert into SalesPriceList  
   ([Company],[Division],[PriceList],[SalesCampaign],[Currency],  
   [CustomerNumber],[EntityType1],[Entitycode1],[EntityType2],[Entitycode2],  
   [EntityType3],[Entitycode3],[UOM],[ExchangeRateType],[StyleNumber],  
   [MerchandiseHierarchy],[ValidFrom],[ItemNumber],[NetPriceUsed],[BonusGenerating],  
   [CommissionGenerating],[SalesPrice],[SalesPriceNotRounded],[SalesPriceQuantity],[SalesPriceUnitOfMeasure],  
   [SelectionMatrix],[ScaleU/M],[ValidTo],[VATIncluded],[VATCode],  
   [PriceIndex],[ObjectAccessGroup],[Formula],[ResultIdentity],[CreateDate],  
   [CreateTime],[ChangeNo],[ChangedBy],[StoreTimeStamp],[EASTimeStamp],[BatchNumber])  
 select @company, @division, WO.KSCHL, Case when WO.KSCHL = 'ZVK1' then '2' else '0' end,CASE WHEN WO.CURCY ='' THEN 'PHP' ELSE WO.CURCY END ,  
   'NA','OBWHLO',wo.LOCNR,'NA','NA',  
   'NA','NA',WO.MEINH,'','',  
   '',WO.DATAB,WO.MATNR,0,0,  
   0,WO.KWERT,0,0,'',  
   '','',WO.DATBI,0,0,  
   0,'','','',WO.CREDAT,  
   WO.CRETIM,1,'SAPMGP',0,  
   @TIMESTAMP,'0'       
   
   --SELECT DISTINCT [LOCNR ]
   from StagingDB_Sonak.dbo.in_pos_article_master  WO  WHERE MATNR='ONIT-1183A701017' and kschl<>''
  -- ORDER by [LOCNR ]

end