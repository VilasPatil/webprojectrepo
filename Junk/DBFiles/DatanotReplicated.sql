select ROW_NUMBER() over (order by WarehouseID) as SrNo,WarehouseID into #TmpStores from Store where IsActive='1'
DECLARE @Counter INT 
DECLARE @TotalRecords INT

select @TotalRecords=count(*) from #TmpStores
--drop table tmpPendingDataWarhouse
--drop table #TmpStores
create table tmpPendingDataWarhouse
(
intid int identity(1,1),
Warehouse varchar(10),
Tablename varchar(100)
)
declare @Warehouse varchar(10)
SET @Counter=1
WHILE ( @Counter <= @TotalRecords )
BEGIN
   
    
select @Warehouse= WarehouseID from #TmpStores where SrNo=@Counter

--set @Warehouse='ABH'
insert into tmpPendingDataWarhouse(Warehouse,Tablename)
select @Warehouse as Warehouse,DestinationTable from ReplicationTask where direction =4 and TaskEnable=1
and DestinationTable not in (
select TableName from RSFBatchSentLog where warehouse=@Warehouse and (CreateDate>=20210103))

SET @Counter  = @Counter  + 1
END

select * from tmpPendingDataWarhouse

