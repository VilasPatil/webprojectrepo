If exists(select top 1 1 from sys.objects where object_id = OBJECT_ID(N'CashOrderTrnListForReturn_X') and type in ('P','PC'))
BEGIN
DROP PROCEDURE CashOrderTrnListForReturn_X
END
GO
/****** Object:  StoredProcedure [dbo].[CashOrderTrnListForReturn_X]    Script Date: 1/13/2021 6:27:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CashOrderTrnListForReturn_X]                                
(    
@Company AS  Smallint,                                       
@Division AS  nvarchar(3),                                      
@PassedInvoiceNumber AS  nvarchar(30),                                        
@Warehouse AS nvarchar(5)='',                                  
@InvYear AS Decimal(8,0)                            
)    
AS                                      
--exec CashOrderTrnListForReturn_X 999, 888,'1953', 'US01', '2015'   
/* ID   AuthorID   DtChange    ModID/IssueID      Method Name    Description */        
/* 1C   BRAPE01 03-02-2017       Add Coloum Name Counter Number in #TemporaryTable and return that coloum in select    
 2C   BVIPA07 23-03-2017       put join on cashorderTrn and CashOrderTrn_push  table  
 3C  BJESH01  05-04-2017      optimization
 4C	 BRAPE01   01-11-2017		 R&D-17764	   			Added Innner JOIN with Warehouse table and checked GoodSender should not be equal to 2 
 1S	 BVIPA02   13-01-2021		 AISB-88763	   			 Added invoicenumber in where clause.
 */       
BEGIN                                      

CREATE TABLE #TemporaryTable                                        
(                                      
	InvoiceNumber numeric(9,0) ,                                      
	CustomerName  nvarchar(50),                                      
	InvoiceYear Decimal(8,0),                            
	CheckValue NVARCHAR(5),             
	Warehouse nvarchar(5),                    
	CreateDate int,    
	/*START 1C*/    
	CounterNo int    
	/*END 1C*/    
)                                                                                                 

--Declare @InvoiceQty Numeric(15,6)                                          
--Declare @ReturnQty Numeric(10,6)                                      
Declare @InvoiceNumber numeric(9,0)                                       
Declare @SalesReturnNumber nvarchar(20)                                      
Declare @CustomerNo   nvarchar(20)                                      
Declare @CustomerName nvarchar(50)                                      
Declare @InvoiceYear Decimal(8,0)                                       
Declare @StoreCode nvarchar(5)                                    
Declare @CreateDate int                        
Declare @ReedemWarehouse nvarchar(20)    
 

IF ltrim(rtrim(@PassedInvoiceNumber )) = ''AND ltrim(rtrim(@Warehouse )) ='' AND ltrim(rtrim(@InvYear)) =0                           
 BEGIN      

	/*Start-2C*/  
	--SELECT InvoiceNumber,InvoiceYear,Warehouse,CreateDate FROM [Cashordertrn](nolock)  
	SELECT [Cashordertrn].InvoiceNumber,[Cashordertrn].InvoiceYear,[Cashordertrn].Warehouse,[Cashordertrn].CreateDate   
	FROM [Cashordertrn](nolock)   
	LEFT OUTER join [CashOrderTrn_Push](nolock)  on  
	Cashordertrn.Company= CashOrderTrn_Push.Company And  
	Cashordertrn.Division= CashOrderTrn_Push.Division And  
	Cashordertrn.Warehouse= CashOrderTrn_Push.Warehouse And  
	Cashordertrn.InvoiceNumber= CashOrderTrn_Push.InvoiceNumber AND  
	Cashordertrn.InvoiceYear= CashOrderTrn_Push.InvoiceYear  
	/*End-2C*/  
	/*START-4C*/
	INNER JOIN Warehouse W ON

	Cashordertrn.Company = W.Company AND

	CashOrderTrn.Division = W.Division AND

	CashOrderTrn.Warehouse = W.Warehouse AND

	W.GoodsSender <> '2' 
	/*END-4C*/
	where  0 = 1     

END      
 

ELSE             
begin     

	/*Start-3C*/
	-- Following Index is added by Jeevan on 29th March 2017 for Optimisation Starts


	if not exists(select 1 from Sys.indexes I where [object_id] = object_id('CashOrderTrn') and Name = 'IX_CashOrderTrnListForReturn')
		Create NonClustered index IX_CashOrderTrnListForReturn on CashOrderTrn
		(
			Company, Division, Warehouse, InvoiceNumber, InvoiceYear , CreateDate
		)
    -- Following Index is added by Jeevan on 29th March 2017 for Optimisation Ends

	           /*End-3C*/                  
	Insert into #TemporaryTable (InvoiceNumber, CustomerName, InvoiceYear, Warehouse, CreateDate, /*START 1C*/CounterNo /*END 1C*/)  
	
	-- Following code is added by Jeevan on 29th March 2017 for Optimisation Starts 
	/*Start-3C*/
	--Select
	


	Select 
	Distinct 
	CashOrderTrn.InvoiceNumber,     
	Customer.CustomerName,    
	CashOrderTrn.InvoiceYear,    
	CashOrderTrn.Warehouse,    
	CashOrderTrn.CreateDate,    
	cashorderTrn.CounterNumber     
	from CashOrderTrn with(NOLOCK) 
	INNER JOIN 
	(
		Select Distinct Company , CustomerNumber ,CustomerName from Customer with(NOLOCK) 
		Union
		Select Distinct Company , CustomerNumber ,CustomerName from CustomerCardInfo with(NOLOCK) 

	) Customer ON  Customer.Company = CashOrderTrn.Company AND Customer.Customernumber = CashOrderTrn.CustomerNumber
	/*START-4C*/
	INNER JOIN Warehouse W ON
	Cashordertrn.Company = W.Company AND
	CashOrderTrn.Division = W.Division AND
	CashOrderTrn.Warehouse = W.Warehouse AND
	W.GoodsSender <> '2' 
	--and CashOrderTrn.Warehouse=@Warehouse
	and CashOrderTrn.InvoiceNumber= @PassedInvoiceNumber /*1S*/
	/*END-4C*/
	left join BillCancellationTrn B on CashOrderTrn.Company = B.Company And CashOrderTrn.Division = B.Division And CashOrderTrn.Warehouse = B.Warehouse And CashOrderTrn.InvoiceNumber = B.InvoiceNumber And 
													      CashOrderTrn.InvoiceYear = B.InvoiceYear AND CashOrderTrn.CreateDate = B.CreateDate 
														  AND CashOrderTrn.AdvanceOrderNumber = 0
	WHERE 
	CashOrderTrn.InvoiceType = '31' and     
	CashOrderTrn.Company = @company AND    
	CashOrderTrn.Division = @Division AND   
	CashOrderTrn.TransactionStatus IN (0, 9) AND     
	CashOrderTrn.FreeField2 = '0' and    
	(CashOrderTrn.InvoiceQuantity-CashOrderTrn.ReturnQuantity)>0 and B.InvoiceNumber is null

	Union 

	Select 
	Distinct 
	CashOrderTrn.InvoiceNumber,     
	Customer.CustomerName,    
	CashOrderTrn.InvoiceYear,    
	CashOrderTrn.Warehouse,    
	CashOrderTrn.CreateDate,    
	cashorderTrn.CounterNumber     
	from CashOrderTrn_Push CashOrderTrn with(NOLOCK) 
	INNER JOIN 
	(
		Select Distinct Company , CustomerNumber ,CustomerName from Customer with(NOLOCK) 
		Union
		Select Distinct Company , CustomerNumber ,CustomerName from CustomerCardInfo with(NOLOCK) 

	) Customer ON  Customer.Company = CashOrderTrn.Company AND Customer.Customernumber = CashOrderTrn.CustomerNumber
	/*START-4C*/
	INNER JOIN Warehouse W ON

	Cashordertrn.Company = W.Company AND

	CashOrderTrn.Division = W.Division AND

	CashOrderTrn.Warehouse = W.Warehouse AND


	W.GoodsSender <> '2' 
	--and CashOrderTrn.Warehouse=@Warehouse
	and CashOrderTrn.InvoiceNumber= @PassedInvoiceNumber /*1S*/
	/*END-4C*/
	left join BillCancellationTrn B on CashOrderTrn.Company = B.Company And CashOrderTrn.Division = B.Division And CashOrderTrn.Warehouse = B.Warehouse And CashOrderTrn.InvoiceNumber = B.InvoiceNumber And 
													      CashOrderTrn.InvoiceYear = B.InvoiceYear AND CashOrderTrn.CreateDate = B.CreateDate 
														  AND CashOrderTrn.AdvanceOrderNumber = 0
	WHERE 
	CashOrderTrn.InvoiceType = '31' and     
	CashOrderTrn.Company = @company AND    
	CashOrderTrn.Division = @Division AND   
	CashOrderTrn.TransactionStatus IN (0, 9) AND     
	CashOrderTrn.FreeField2 = '0' and    
	B.InvoiceNumber is null

END                                  
 
BEGIN   
                                   
	IF @PassedInvoiceNumber <> ''  AND @InvYear = 0 AND @Warehouse = ''    
	BEGIN                         
		Select distinct InvoiceNumber,    
		CustomerName ,    
		InvoiceYear,    
		Warehouse,    
		CreateDate,    
		/*START 1C*/    
		CounterNo    
		/*END 1C*/    
		--ReturnQuantity,    
		--BalanceQuantity     
		from #TemporaryTable with(NOLOCK)    
		Where (InvoiceNumber  LIKE '%'+  CONVERT(nvarchar(10),@PassedInvoiceNumber) + '%' OR     
		CustomerName like '%' +ltrim(rtrim(@PassedInvoiceNumber)) + '%' )    
		order by InvoiceYear desc, InvoiceNumber desc ,Warehouse desc    
	END                                                          

	IF @PassedInvoiceNumber <> ''  AND @InvYear <> 0 AND @warehouse <> ''                                   
	BEGIN                                    
		Select distinct InvoiceNumber,    
		CustomerName,    
		InvoiceYear,    
		Warehouse,    
		CreateDate,    
		/*START 1C*/    
		CounterNo     
		/*END 1C*/    
		from #TemporaryTable (NOLOCK)                                 
		Where (InvoiceNumber  LIKE '%'+  CONVERT(nvarchar(10),@PassedInvoiceNumber) + '%' OR     
		CustomerName like '%' +ltrim(rtrim(@PassedInvoiceNumber)) + '%' ) AND      
		InvoiceYear = @InvYear AND    
		Warehouse = @Warehouse    order by InvoiceYear desc, InvoiceNumber desc ,Warehouse desc                                    
	END    
	                                
	IF @PassedInvoiceNumber = ''  AND @InvYear <> 0 AND @warehouse <> ''                                     
	BEGIN                                    
		Select distinct InvoiceNumber,    
		CustomerName,    
		InvoiceYear,    
		Warehouse,    
		CreateDate, /*START 1C*/CounterNo  /*END 1C*/    
		from #TemporaryTable (NOLOCK)     
		Where InvoiceYear = @InvYear     
		AND warehouse = @Warehouse    
		order by InvoiceYear desc, InvoiceNumber desc ,Warehouse desc                                   
	END      
 
	IF @PassedInvoiceNumber <>''  AND @InvYear = 0 AND @warehouse <> ''                                     
	BEGIN                                    
		Select distinct InvoiceNumber,    
		CustomerName,    
		InvoiceYear,    
		Warehouse,    
		CreateDate,    
		/*START 1C*/    
		CounterNo     
		/*END 1C*/    
		from #TemporaryTable (NOLOCK)     
		Where (InvoiceNumber  LIKE '%'+  CONVERT(nvarchar(10),@PassedInvoiceNumber) + '%' OR     
		CustomerName like '%' +ltrim(rtrim(@PassedInvoiceNumber)) + '%' ) AND     
		Warehouse = @Warehouse    
		order by InvoiceYear desc, InvoiceNumber desc ,Warehouse desc                     
	END      

	IF @PassedInvoiceNumber =''  AND @InvYear = 0 AND @warehouse <> ''                                     
	BEGIN                                    
		Select distinct InvoiceNumber,    
		CustomerName,    
		InvoiceYear,    
		Warehouse,    
		CreateDate,    
		/*START 1C*/    
		CounterNo     
		/*END 1C*/    
		from #TemporaryTable (NOLOCK)     
		Where      
		Warehouse = @Warehouse    
		order by InvoiceYear desc, InvoiceNumber desc ,Warehouse desc                     
	END    

	IF @PassedInvoiceNumber =''  AND @InvYear <> 0 AND @warehouse = ''                                     
	BEGIN                                    
		Select distinct InvoiceNumber,    
		CustomerName,    
		InvoiceYear,    
		Warehouse,    
		CreateDate,    
		/*START 1C*/    
		CounterNo     
		/*END 1C*/    
		from #TemporaryTable (NOLOCK)     
		Where      
		invoiceyear = @InvoiceYear    
		order by InvoiceYear desc, InvoiceNumber desc ,Warehouse desc                     
	END  
	
	IF @PassedInvoiceNumber <>''  AND @InvYear <> 0 AND @warehouse = ''                                     
	BEGIN                                    
		Select distinct InvoiceNumber,    
		CustomerName,    
		InvoiceYear,    
		Warehouse,    
		CreateDate,    
		/*START 1C*/    
		CounterNo    
		/*END 1C*/     
		from #TemporaryTable (NOLOCK)     
		Where (InvoiceNumber  LIKE '%'+  CONVERT(nvarchar(10),@PassedInvoiceNumber) + '%' OR     
		CustomerName like '%' +ltrim(rtrim(@PassedInvoiceNumber)) + '%' ) AND     
		InvoiceYear = @InvYear    
		order by InvoiceYear desc, InvoiceNumber desc,warehouse desc                                    
	END      

END                                      
 
End