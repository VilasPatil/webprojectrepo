<%@ Language="VBscript"%>
<%
Option Explicit
%>
<%
Server.ScriptTimeout = 1800
Response.Expires = 0
Response.Expiresabsolute = Now() - 1
Response.AddHeader "pragma","no-cache"
Response.CacheControl = "no-cache"
Response.ContentType = "text/xml"
%>
<!--#INCLUDE FILE="common.asp"-->
<%
Dim strXML
Dim strStoreName
Dim strFolderName
Dim strFile
Dim strFilePath
DIm byteSafeArray 
Dim ObjFileSys
Dim ObjStream

strStoreName = Request.QueryString("Store")
strFolderName = Request.QueryString("Folder")
strFile = Request.QueryString("File")

Set ObjFileSys = Server.CreateObject("Scripting.FileSystemObject")

If Not ObjFileSys.FolderExists(strRootPath & "\" & strStoreName) Then
	Response.Write "<XML><ERROR>" & strStoreName & " Folder is missing at EAS</ERROR></XML>"
	Response.End
End If

If Not ObjFileSys.FolderExists(strRootPath & "\" & strStoreName & "\" & strInboxName) Then
	Response.Write "<XML><ERROR>Inbox folder within the store fodler " & strStoreName & " is missing at EAS</ERROR></XML>"
	Response.End
End If

If Not ObjFileSys.FolderExists(strRootPath & "\" & strStoreName & "\" & strInboxName & "\" & strFolderName) Then
	Call ObjFileSys.CreateFolder(strRootPath & "\" & strStoreName & "\" & strInboxName & "\" & strFolderName)
End If

If Not ObjFileSys.FileExists(strRootPath & "\" & strStoreName & "\" & strInboxName & "\" & strFolderName & "\" & strFile) Then
	'ObjFileSys.DeleteFile strRootPath & "\" & strStoreName & "\" & strInboxName & "\" & strFolderName & "\" & strFile
    strFilePath = strRootPath & "\" & strStoreName & "\" & strInboxName & "\" & strFolderName & "\" & strFile
	byteSafeArray = Request.BinaryRead(Request.TotalBytes)
    Set ObjStream = Server.CreateObject("ADODB.Stream")
    ObjStream.Mode = 3 
    ObjStream.Open
    ObjStream.Type = 1 
    ObjStream.Write byteSafeArray
    ObjStream.Position = 0
    ObjStream.SaveToFile strFilePath , 1 
    '-- below line written by bhavesh suthar for checking if files are getting deleted from new location or not...
    'ObjStream.SaveToFile strRootPath & "\" & strStoreName & "\" & strfoldername& "_" & strFile, 1 
    ObjStream.Close
    Set ObjStream = Nothing

    strXML = "<XML><STATUS>" & strFile & "</STATUS></XML>"

End If

Response.write strXML
%>
